# sq-devops-wiki


## Overview

* Outline the process of preparing AMI images ahead of deployment
* Define steps required to launch a new environment
* Link-up other repositories related to the deployment process

## Overall Diagram

![](./media/deployment.jpg)

## Sequence of AMI Preparation and Deployment

### AMI's Preparation

* AMI preparation is taking a longer than few minutes time, especially for Sql Server, due to a large size of the backup file
* Additionally stateless servers will be added to an auto-scale group at some future point and running long provisioning is not feasible
* The AMI preparation process does not include building the source code and the assumption that CI/CD processes will be addressed at a later stage
* [Web Server](../../../sq-web-server-desktop) and [API Server](../../../sq-api-server) repositories are preparing Windows Server 2016 instances to be launched just-in-time with configuration settings populated on EC2 instance launch
* [Sql Server](../../../sq-sql-server) repository is restoring a backup from S3 bucket - an overnight process, and, later, is used to create AMI image for Sql Server deployment

### Terraform Deployment

1. Use [pipeline project](../../../sq-devops-pipeline) to deploy linked repositories